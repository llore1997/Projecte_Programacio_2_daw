<?php



require($_SERVER['DOCUMENT_ROOT'] . "/Cerveza/modules/products_frontend/model/BLL/products_bll.class.singleton.php");
class products_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = products_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_products($limit) {
        return $this->bll->list_products_BLL($limit);
    }

    public function details_products($id) {

        return $this->bll->details_products_BLL($id);
    }

}
