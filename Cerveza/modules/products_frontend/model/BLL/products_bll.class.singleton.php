<?php

require($_SERVER['DOCUMENT_ROOT'] . "/Cerveza/model/Db.class.singleton.php");
require($_SERVER['DOCUMENT_ROOT'] . "/Cerveza/modules/products_frontend/model/DAO/products_dao.class.singleton.php");

class products_bll {
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = products_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_products_BLL($limit) {
        return $this->dao->list_products_DAO($this->db,$limit);
    }

    public function details_products_BLL($id) {
        return $this->dao->details_products_DAO($this->db,$id);
    }

}
